#include <ros/ros.h>
#include <pf_nav/sonar_sim_class.h>

int main(int argc, char **argv) {
  ros::init(argc, argv, "sonar_sim");
  ros::NodeHandle n;
  SonarSim sonar_sim(n);
  if (ros::Time::isSimTime()) {
      ros::Time::waitForValid();
  }
  // test if sim, wait for time > 0
  sonar_sim.Init();
  while (ros::ok()) {
    ros::spin();
  }
  return 0;
}
