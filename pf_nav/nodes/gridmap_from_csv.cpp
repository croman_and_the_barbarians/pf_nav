#include <ros/ros.h>
#include <pf_nav/gridmap_from_csv_class.h>

int main(int argc, char **argv) {
  ros::init(argc, argv, "gridmap_maker");
  ros::NodeHandle n;
  GridmapMakerCsv gridmap_maker(n);

  return 0;
}
