#include "pf_nav/pf_nav_class.h"
#include <bfl/wrappers/rng/rng.h>

// globals used in measurements and updates, switch to singletons?
double dt; // this is used to get dt into the update function.
double hdg_offset;
grid_map::GridMap gridmap;
double last_vehicle_depth;
//double last_accel_x;
//double last_accel_y;
double last_yaw;
double last_vx;
double last_vy;
double last_vx_var;
double last_vy_var;
double last_yaw_var;

namespace BFL
{

  VehicleModel::VehicleModel(const BFL::Gaussian& additiveNoise): BFL::ConditionalPdf<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>(N_STATES, 1) {
    _additiveNoise = additiveNoise;
  }

  VehicleModel::~VehicleModel() {}

  bool VehicleModel::SampleFrom(BFL::Sample<MatrixWrapper::ColumnVector>& one_sample, int method, void* args) const {
    MatrixWrapper::ColumnVector state = ConditionalArgumentGet(0);

    // 1 = x position (global frame)
    // 2 = y position (global frame)
    // 3 = x velocity (vehicle frame)
    // 4 = y velocity (vehicle frame)
    // 5 = heading offset (radians, unwrapped)
    double vx = last_vx + state(3);
    double vy = last_vy + state(4);
    double hdg = last_yaw + state(5) + hdg_offset;

    state(1) += (cos(hdg) * vx - sin(hdg) * vy)* dt;
    state(2) += (cos(hdg) * vy + sin(hdg) * vx)* dt;
    //state(3) += last_accel_x * dt;
    //state(4) += last_accel_y * dt;
    state(3) = 0; // no more additive noise, reset each iteration
    state(4) = 0;
    state(5) = 0;

    BFL::Sample<MatrixWrapper::ColumnVector> noise;
    _additiveNoise.SampleFrom(noise, method, args);

    one_sample.ValueSet(state + noise.ValueGet());
    return true;
  }

}; // end namespace BFL


PfNav::PfNav(ros::NodeHandle &n) {
  nh_ = n;
  params.getParams(nh_);

  // load grid map
  ROS_INFO("Loading %s", params.gridmap_filename.c_str());
  grid_map::GridMapRosConverter::loadFromBag(params.gridmap_filename, params.gridmap_topic, gridmap);

}

void PfNav::InitFilter() {
  gridmap.setTimestamp(ros::Time::now().toNSec());
  grid_map::GridMapRosConverter::toMessage(gridmap, gridmap_msg);
  gridmap_pub = nh_.advertise<grid_map_msgs::GridMap>(params.gridmap_topic, 1, true);
  gridmap_pub.publish(gridmap_msg);
  hdg_offset = params.heading_offset;
  last_vehicle_depth = 0.0;
  //last_accel_x = 0.0;
  //last_accel_y = 0.0;
  last_vx = 0.0;
  last_vy = 0.0;
  last_yaw = 0.0;
  last_range_valid = false;

  // ros subscribers and publishers
  mean_odometry_pub = nh_.advertise<nav_msgs::Odometry>(params.odometry_topic, 10);
  point_cloud_pub = nh_.advertise<sensor_msgs::PointCloud>(params.pointcloud_topic, 10);
  particle_cloud_pub = nh_.advertise<pf_nav_msgs::ParticleCloud>(params.particlecloud_topic, 10);

  //tf_listener_ = new tf2_ros::TransformListener(tf_buffer_);

  if (params.use_visualodom) {
    visual_odometry_sub = nh_.subscribe(params.visualodom_topic, 10, &PfNav::VisualOdometryCallback, this);
  }
    else if (params.use_acfrvisualodom) {
      visual_odometry_sub = nh_.subscribe(params.visualodom_topic, 10, &PfNav::ACFRVisualOdometryCallback, this);
    }
  if (params.use_range) {
    range_sub = nh_.subscribe(params.range_topic, 10, &PfNav::RangeCallback, this);
  }
    else if (params.use_acfrrange) {
      range_sub = nh_.subscribe(params.range_topic, 10, &PfNav::ACFRRangeCallback, this);
      last_vehicle_depth = 0.;
    }
  if (params.use_imu) {
    imu_sub = nh_.subscribe(params.imu_topic, 10, &PfNav::ImuCallback, this);
  }
  if (params.use_vehicledepth) {
    vehicledepth_sub = nh_.subscribe(params.vehicledepth_topic, 10, &PfNav::VehicleDepthCallback, this);
  }
  if  (params.use_dvl) {
    dvl_sub = nh_.subscribe(params.dvl_topic, 10, &PfNav::DvlCallback, this);
  }
  if (params.use_usbl) {
    usbl_sub = nh_.subscribe(params.usbl_topic, 10, &PfNav::UsblCallback, this);
  }
  if (params.use_deadreckon) {
    deadreckon_sub = nh_.subscribe(params.deadreckon_topic, 10, &PfNav::DeadreckonCallback, this);
  }

  //BFL::Gaussian sys_uncertainty(sys_noise_mu, sys_noise_cov);
  //sys_pdf = new BFL::VehicleModel(sys_uncertainty);
  //sys_model = new BFL::SystemModel<MatrixWrapper::ColumnVector>(sys_pdf);


  // set prior distribution
  MatrixWrapper::ColumnVector prior_mu(N_STATES);
  for (uint i=0; i<5; i++) {
    prior_mu(i+1) = params.prior_mu[i];
  }
  // move prior heading to last_yaw
  last_yaw = prior_mu(5);
  prior_mu(5) = 0.0;

  MatrixWrapper::SymmetricMatrix prior_cov(N_STATES);
  prior_cov = 0.0;
  for (uint i=0; i<5; i++) {
    for (uint j=0; j<5; j++) {
      prior_cov(i+1, j+1) = params.prior_cov[N_STATES*i + j];

    }
  }
  BFL::Gaussian prior_cont(prior_mu, prior_cov);
  std::vector<BFL::Sample<MatrixWrapper::ColumnVector>> prior_samples(params.n_particles);
  prior_discr = new BFL::MCPdf<MatrixWrapper::ColumnVector>(params.n_particles, N_STATES);
  prior_cont.SampleFrom(prior_samples, int(params.n_particles), CHOLESKY, nullptr);
  prior_discr->ListOfSamplesSet(prior_samples);

  // bootstrap filter
  filter = new BFL::BootstrapFilter<MatrixWrapper::ColumnVector,MatrixWrapper::ColumnVector>(prior_discr, 0, 0.25*params.n_particles, 0);
  //filter = new BFL::BootstrapFilter<MatrixWrapper::ColumnVector,MatrixWrapper::ColumnVector>(prior_discr, 10, 0, 0); // timed resampling, threshold resampling, resampling method
  // 0 = multinomial
  // 1 = systematic
  // 2 = stratified
  // 3 = residual
  // 4 = minimum variance
  lastUpdate = ros::Time::now();
  lastImuUpdate = ros::Time::now();
  lastWaterdepthUpdate = ros::Time::now();
  lastDvlUpdate = ros::Time::now();
  waterdepthInterval = ros::Duration(params.delta_t);
  imuInterval = ros::Duration(params.delta_t);
  dvlInterval = ros::Duration(params.delta_t);

  // start timer for main loop
  timer_for_callback = nh_.createTimer(ros::Duration(params.delta_t), &PfNav::timerCallback, this);
}

PfNav::~PfNav() {
  //delete sys_model;
  delete filter;
}

BFL::Probability BFL::UsblPdf::ProbabilityGet(const MatrixWrapper::ColumnVector& measurement) const {
  MatrixWrapper::ColumnVector state = ConditionalArgumentGet(0);
  MatrixWrapper::ColumnVector expected_measurement(2);
  expected_measurement(1) = state(1); // x
  expected_measurement(2) = state(2); // y
  return _measurementNoise.ProbabilityGet(expected_measurement - measurement);
}

BFL::Probability BFL::VisualOdometryPdf::ProbabilityGet(const MatrixWrapper::ColumnVector& measurement) const {
  MatrixWrapper::ColumnVector state = ConditionalArgumentGet(0);
  MatrixWrapper::ColumnVector expected_measurement(2);
  expected_measurement(1) = state(3); // dx
  expected_measurement(2) = state(4); // dy
  //ROS_INFO("Velocity measured: (%.2f, %.2f).  Particle: (%.2f, %.2f).", measurement(1), measurement(2), expected_measurement(1), expected_measurement(2));

  return _measurementNoise.ProbabilityGet(expected_measurement - measurement);
}

BFL::Probability BFL::RangePdf::ProbabilityGet(const MatrixWrapper::ColumnVector& measurement) const {
  MatrixWrapper::ColumnVector state = ConditionalArgumentGet(0);

  grid_map::Position position(state(1), state(2));
  float waterdepth;
  if (gridmap.isInside(position)) { // segfault-preventer
    waterdepth = gridmap.atPosition("elevation", position, grid_map::InterpolationMethods::INTER_LINEAR);
  }
  else {
    waterdepth = 0.0;
  }
  double range = -waterdepth + last_vehicle_depth;
  //ROS_INFO("Position: (%.2f, %.2f)  Waterdepth: %.2f  Vehicledepth: %.2f  Range: %.2f  Expected: %.2f", state(1), state(2), waterdepth, last_vehicle_depth, measurement(1), range);
  if (range > double(0.001)) { // hacky way to check if we have data, do this better
    MatrixWrapper::ColumnVector expected_measurement(1);
    expected_measurement(1) = double(range);
    return _measurementNoise.ProbabilityGet(expected_measurement - measurement);
  }
  else {
    return 0.0;
  }
}

BFL::Probability BFL::ImuPdf::ProbabilityGet(const MatrixWrapper::ColumnVector& measurement) const {
  MatrixWrapper::ColumnVector state = ConditionalArgumentGet(0);
  //MatrixWrapper::ColumnVector expected_measurement(1);
  //expected_measurement = state(5); // heading

  double heading_error = state(5) - measurement(1);

  //ROS_INFO("IMU heading %f, particle %f, error %f", measurement(1), state(5), heading_error);
  MatrixWrapper::ColumnVector measurement_error(1);
  measurement_error(1) = heading_error;
  return _measurementNoise.ProbabilityGet(measurement_error);
}

void PfNav::VehicleDepthCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg) {
  //ROS_INFO("Vehicle depth callback.");
  last_vehicle_depth = msg->pose.pose.position.z;
}


void PfNav::UsblCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg) {
  if (msg->pose.covariance[0] < 0.01) {
    return;
  }
  MatrixWrapper::ColumnVector measurement(2);
  measurement(1) = msg->pose.pose.position.x;
  measurement(2) = msg->pose.pose.position.y;

  // new measurement model for this covariance
  MatrixWrapper::ColumnVector usbl_noise_Mu(2);
  usbl_noise_Mu = 0.0;
  MatrixWrapper::SymmetricMatrix usbl_noise_Cov(2);
  usbl_noise_Cov = 0.0;
  usbl_noise_Cov(1,1) = pow(msg->pose.covariance[0], 0.5); // x:x
  usbl_noise_Cov(2,2) = pow(msg->pose.covariance[7], 0.5); // y:y
  BFL::Gaussian usbl_uncertainty(usbl_noise_Mu, usbl_noise_Cov);
  usbl_pdf = new BFL::UsblPdf(usbl_uncertainty);
  usbl_measurement_model = new BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>(usbl_pdf);

  filter->Update(usbl_measurement_model, measurement);

  // clean up
  delete usbl_pdf;
  delete usbl_measurement_model;
}

void PfNav::DvlCallback(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& msg) {
  //if (msg->header.stamp < lastDvlUpdate + dvlInterval) {
  //  return;
  //}
  //lastDvlUpdate = msg->header.stamp;

  // get measurement from twist message
  //ROS_INFO("Got visual odometry message.");
  //ROS_INFO("Got DVL message.");
  //MatrixWrapper::ColumnVector measurement(2);
  // hacky rotation, replace with URDF
  // should filter these.
  //measurement(1) = msg->twist.twist.linear.z; // real X = DVL Z
  //measurement(2) = -msg->twist.twist.linear.x; // real Y = DVL -X
  // no measurement 3, but real Z = DVL Y
  last_vx = msg->twist.twist.linear.z; // real X = DVL Z
  last_vy = -msg->twist.twist.linear.x; // real Y = DVL -X
  last_vx_var = pow(msg->twist.covariance[14], 0.5);
  last_vy_var = pow(msg->twist.covariance[0], 0.5);

  // new measurement model for this covariance
  //MatrixWrapper::ColumnVector visual_odometry_noise_Mu(2);
  //visual_odometry_noise_Mu = 0.0; // zero mean
  //MatrixWrapper::SymmetricMatrix visual_odometry_noise_Cov(2);
  //visual_odometry_noise_Cov = 0.0;
  // hacky rotation, replace with URDF
  //visual_odometry_noise_Cov(1,1) = pow(msg->twist.covariance[14], 0.5); // vx:vx
  //visual_odometry_noise_Cov(1,2) = pow(msg->twist.covariance[1], 0.5); // vx:vy
  //visual_odometry_noise_Cov(2,1) = pow(msg->twist.covariance[6], 0.5); // vy:vx
  //visual_odometry_noise_Cov(2,2) = pow(msg->twist.covariance[0], 0.5); // vy:vy
  // identical to visual odometry for now, may change if we add Z.
  //BFL::Gaussian visual_odometry_uncertainty(visual_odometry_noise_Mu, visual_odometry_noise_Cov);
  //visual_odometry_pdf = new BFL::VisualOdometryPdf(visual_odometry_uncertainty);
  //visual_odometry_measurement_model = new BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>(visual_odometry_pdf);

  //filter->Update(visual_odometry_measurement_model, measurement);

  // clean up
  //delete visual_odometry_pdf;
  //delete visual_odometry_measurement_model;
}

void PfNav::DeadreckonCallback(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& msg) {
  //if (msg->header.stamp < lastDvlUpdate + dvlInterval) {
  //  return;
  //}
  //lastDvlUpdate = msg->header.stamp;

  // get measurement from twist message
  //ROS_INFO("Got deadreckon message.");
  //MatrixWrapper::ColumnVector measurement(2);
  //measurement(1) = msg->twist.twist.linear.x;
  //measurement(2) = msg->twist.twist.linear.y;
  //last_vx = measurement(1);
  //last_vy = measurement(2);

  last_vx = msg->twist.twist.linear.x;
  last_vy = msg->twist.twist.linear.y;
  last_vx_var = pow(msg->twist.covariance[0], 0.5);
  last_vy_var = pow(msg->twist.covariance[7], 0.5);


  // new measurement model for this covariance
  //MatrixWrapper::ColumnVector visual_odometry_noise_Mu(2);
  //visual_odometry_noise_Mu = 0.0; // zero mean
  //MatrixWrapper::SymmetricMatrix visual_odometry_noise_Cov(2);
  //visual_odometry_noise_Cov = 0.0;
  // hacky rotation, replace with URDF
  //visual_odometry_noise_Cov(1,1) = pow(msg->twist.covariance[14], 0.5); // vx:vx
  //visual_odometry_noise_Cov(1,2) = pow(msg->twist.covariance[1], 0.5); // vx:vy
  //visual_odometry_noise_Cov(2,1) = pow(msg->twist.covariance[6], 0.5); // vy:vx
  //visual_odometry_noise_Cov(2,2) = pow(msg->twist.covariance[0], 0.5); // vy:vy
  // identical to visual odometry for now, may change if we add Z.
  //BFL::Gaussian visual_odometry_uncertainty(visual_odometry_noise_Mu, visual_odometry_noise_Cov);
  //visual_odometry_pdf = new BFL::VisualOdometryPdf(visual_odometry_uncertainty);
  //visual_odometry_measurement_model = new BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>(visual_odometry_pdf);

  //filter->Update(visual_odometry_measurement_model, measurement);

  // clean up
  //delete visual_odometry_pdf;
  //delete visual_odometry_measurement_model;
}

void PfNav::VisualOdometryCallback(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& msg) {
  // get measurement from twist message
  //ROS_INFO("Got visual odometry message.");
  MatrixWrapper::ColumnVector measurement(2);
  measurement(1) = msg->twist.twist.linear.x;
  measurement(2) = msg->twist.twist.linear.y;

  // new measurement model for this covariance
  MatrixWrapper::ColumnVector visual_odometry_noise_Mu(2);
  visual_odometry_noise_Mu = 0.0; // zero mean
  MatrixWrapper::SymmetricMatrix visual_odometry_noise_Cov(2);
  visual_odometry_noise_Cov = 0.0;
  visual_odometry_noise_Cov(1,1) = pow(msg->twist.covariance[0], 0.5); // vx:vx
  visual_odometry_noise_Cov(1,2) = pow(msg->twist.covariance[1], 0.5); // vx:vy
  visual_odometry_noise_Cov(2,1) = pow(msg->twist.covariance[6], 0.5); // vy:vx
  visual_odometry_noise_Cov(2,2) = pow(msg->twist.covariance[7], 0.5); // vy:vy
  BFL::Gaussian visual_odometry_uncertainty(visual_odometry_noise_Mu, visual_odometry_noise_Cov);
  visual_odometry_pdf = new BFL::VisualOdometryPdf(visual_odometry_uncertainty);
  visual_odometry_measurement_model = new BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>(visual_odometry_pdf);

  filter->Update(visual_odometry_measurement_model, measurement);

  // clean up
  delete visual_odometry_pdf;
  delete visual_odometry_measurement_model;
}

void PfNav::ACFRVisualOdometryCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  // get measurement from twist message
  double visual_odom_covariance = 0.04;
  //ROS_INFO("Got visual odometry message.");
  MatrixWrapper::ColumnVector measurement(2);
  measurement(1) = msg->twist.twist.linear.x;
  measurement(2) = msg->twist.twist.linear.y;

  // new measurement model for this covariance
  MatrixWrapper::ColumnVector visual_odometry_noise_Mu(2);
  visual_odometry_noise_Mu = 0.0; // zero mean
  MatrixWrapper::SymmetricMatrix visual_odometry_noise_Cov(2);
  visual_odometry_noise_Cov = 0.0;
  visual_odometry_noise_Cov(1,1) = pow(visual_odom_covariance, 0.5); // vx:vx
  visual_odometry_noise_Cov(2,2) = pow(visual_odom_covariance, 0.5); // vy:vy
  BFL::Gaussian visual_odometry_uncertainty(visual_odometry_noise_Mu, visual_odometry_noise_Cov);
  visual_odometry_pdf = new BFL::VisualOdometryPdf(visual_odometry_uncertainty);
  visual_odometry_measurement_model = new BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>(visual_odometry_pdf);

  filter->Update(visual_odometry_measurement_model, measurement);

  // clean up
  delete visual_odometry_pdf;
  delete visual_odometry_measurement_model;
}

void PfNav::ImuCallback(const sensor_msgs::Imu::ConstPtr& msg) {
  double heading_covariance = 0.01; // approx 5.7 degrees std dev
  //double heading_covariance = 0.000076154; // 0.5 degrees std dev

  //ROS_INFO("Got IMU message.");


  tf2::Quaternion quat;
  quat.setX(msg->orientation.x); quat.setY(msg->orientation.y);
  quat.setZ(msg->orientation.z); quat.setW(msg->orientation.w);
  tf2::Matrix3x3 m(quat);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);

  //tf2::Vector3 g(0, 0, 9.80665);
  //tf2::Vector3 g_rot = tf2::quatRotate(quat, g);

  //ROS_INFO("Got IMU message. Roll: %f, Pitch: %f, Yaw: %f.", roll, pitch, yaw);

  //tf2::Vector3 accel_nog(msg->linear_acceleration.x - g_rot.x(),
  //                       msg->linear_acceleration.y - g_rot.y(),
  //                       msg->linear_acceleration.z - g_rot.z());

  //tf2::Vector3 accel_derot = tf2::quatRotate(quat.inverse(), accel_nog);



  double tau = 0.05;
  //last_accel_x = tau * (msg->linear_acceleration.x) + (1 - tau) * last_accel_x;
  //last_accel_y = tau * (msg->linear_acceleration.y) + (1 - tau) * last_accel_y;
  //double last_accel_z = msg->linear_acceleration.z - gravity_rotated.z();
  //ROS_INFO("Raw: (%f, %f, %f). Rot: (%f, %f, %f).", msg->linear_acceleration.x, msg->linear_acceleration.y, msg->linear_acceleration.z, last_accel_x, last_accel_y, last_accel_z);


  // set yaw. This should be filtered.
  // not-so-great unwrap
  while (yaw - last_yaw > M_PI) {
    yaw -= 2*M_PI;
  }
  while (yaw - last_yaw < -M_PI) {
    yaw += 2*M_PI;
  }
  last_yaw = tau * yaw + (1 - tau) * last_yaw;

  //if (msg->header.stamp < lastImuUpdate + imuInterval) {
  //  return;
 // }
  //lastImuUpdate = msg->header.stamp;

  // enforcing yaw offsets being near 0 needs to be done somewhere, this isn't the best place though.
  //MatrixWrapper::ColumnVector measurement(1);
  //measurement(1) = 0.0;
  //MatrixWrapper::ColumnVector yaw_noise_Mu(1);
  //yaw_noise_Mu = 0.0;
  //MatrixWrapper::SymmetricMatrix yaw_noise_Cov(1);
  if (msg->orientation_covariance[8] < 1e-12) { // undefined orientation covariance, handle this better
    //yaw_noise_Cov(1,1) = pow(heading_covariance, 0.5);
    last_yaw_var = pow(heading_covariance, 0.5);
  }
  else {
    //yaw_noise_Cov(1,1) = pow(msg->orientation_covariance[8], 0.5);
    last_yaw_var = pow(msg->orientation_covariance[8], 0.5);
  }
  //BFL::Gaussian yaw_uncertainty(yaw_noise_Mu, yaw_noise_Cov);
  //imu_pdf = new BFL::ImuPdf(yaw_uncertainty);
  //imu_measurement_model = new BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>(imu_pdf);

  //filter->Update(imu_measurement_model, measurement);

  //delete imu_pdf;
  //delete imu_measurement_model;
}

/*void PfNav::VehicleDepthCallback(const float_sensor_msgs::Depth::ConstPtr& msg) {
  //ROS_INFO("Got vehicle depth message %f", msg->depth);
  last_vehicle_depth = msg->depth;
}*/

void PfNav::ACFRRangeCallback(const sensor_msgs::Range::ConstPtr& msg) {
  if (msg->header.stamp < lastWaterdepthUpdate + waterdepthInterval) {
    //ROS_INFO("Got range message, ignoring.");
    return;
  }
  lastWaterdepthUpdate = msg->header.stamp;
  double range_covariance = 1.0;
  //ROS_INFO("Got range message. Range: %f. Vehicle depth: %f.", msg->range, last_vehicle_depth);
  MatrixWrapper::ColumnVector measurement(1);
  measurement(1) = double(msg->range + last_vehicle_depth);

  // new measurement model for this covariance
  MatrixWrapper::ColumnVector range_noise_Mu(1);
  range_noise_Mu = 0.0; // zero mean
  MatrixWrapper::SymmetricMatrix range_noise_Cov(1);
  range_noise_Cov = 0.0;
  range_noise_Cov(1,1) = pow(range_covariance, 0.5);
  BFL::Gaussian range_uncertainty(range_noise_Mu, range_noise_Cov);
  range_pdf = new BFL::RangePdf(range_uncertainty);
  range_measurement_model = new BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>(range_pdf);

  filter->Update(range_measurement_model, measurement);

  // clean up
  delete range_pdf;
  delete range_measurement_model;
}

void PfNav::RangeCallback(const pf_nav_msgs::RangeWithCovariance::ConstPtr& msg) {
  ROS_INFO("Got range message.");
  if (msg->range.range < 0.001) {
    ROS_WARN("Range less than 0, ignoring.");
    last_range_valid = false;
    return;
  }
  last_range_valid = true;
  MatrixWrapper::ColumnVector measurement(1);
  measurement(1) = double(msg->range.range);

  // new measurement model for this covariance
  MatrixWrapper::ColumnVector range_noise_Mu(1);
  range_noise_Mu = 0.0; // zero mean
  MatrixWrapper::SymmetricMatrix range_noise_Cov(1);
  range_noise_Cov = 0.0;
  range_noise_Cov(1,1) = pow(double(msg->covariance), 0.5);
  BFL::Gaussian range_uncertainty(range_noise_Mu, range_noise_Cov);
  range_pdf = new BFL::RangePdf(range_uncertainty);
  range_measurement_model = new BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>(range_pdf);

  filter->Update(range_measurement_model, measurement);

  // clean up
  delete range_pdf;
  delete range_measurement_model;
}

void PfNav::timerCallback(const ros::TimerEvent& event) {
  // do the update
  ros::Time thisUpdate = ros::Time::now();
  ros::Duration d = (thisUpdate - lastUpdate); // no idea what this warning is about
  dt = d.toSec(); // replace with singleton class?
  //dt = params.delta_t;
  //ROS_INFO("dt: %f", dt);

  // system model
  MatrixWrapper::ColumnVector sys_noise_mu(N_STATES);
  sys_noise_mu = 0.0;
  for (uint i=0; i<5; i++) {
    sys_noise_mu(i+1) = params.sys_noise_mu[i];
  }
  MatrixWrapper::SymmetricMatrix sys_noise_cov(N_STATES);
  sys_noise_cov = 0.0;
  for (uint i=0; i<5; i++) {
    for (uint j=0; j<5; j++) {
      sys_noise_cov(i+1, j+1) = params.sys_noise_cov[N_STATES*i + j];
    }
  }
  sys_noise_cov(3,3) = last_vx_var;
  sys_noise_cov(4,4) = last_vy_var;
  sys_noise_cov(5,5) = last_yaw_var;

  BFL::Gaussian sys_uncertainty(sys_noise_mu, sys_noise_cov);
  sys_pdf = new BFL::VehicleModel(sys_uncertainty);
  sys_model = new BFL::SystemModel<MatrixWrapper::ColumnVector>(sys_pdf);

  filter->Update(sys_model);
  lastUpdate = thisUpdate;

  // clean up sys model uncertainties
  delete sys_pdf;
  delete sys_model;

  // publish the particles
  publishParticles();
  publishParticleCloud(); // publish all particles as a ParticleCloud
  publishOdometry();
  // publishMostLikelyOdometry();
}

void PfNav::publishParticles() {
  // publish a ROS PointCloud of particle positions X,Y,Z
  // swap this out with TwistyPoseArray after writing rviz plugin
  //ROS_INFO("This is where the particles will be published.");
  std::vector<BFL::WeightedSample<MatrixWrapper::ColumnVector>>::iterator sample_itr;
  std::vector<BFL::WeightedSample<MatrixWrapper::ColumnVector>> samples;
  samples = filter->PostGet()->ListOfSamplesGet();

  sensor_msgs::PointCloud pointcloud_msg;
  pointcloud_msg.header.frame_id = params.map_frame;
  pointcloud_msg.header.stamp = ros::Time::now();

  sensor_msgs::ChannelFloat32 channels;
  channels.name = "weight";

  for (sample_itr = samples.begin(); sample_itr<samples.end(); sample_itr++) {
    geometry_msgs::Point32 point;
    MatrixWrapper::ColumnVector sample = (*sample_itr).ValueGet();
    point.x = float(sample(1));
    point.y = float(sample(2));
    point.z = last_vehicle_depth - 0.5;
    pointcloud_msg.points.insert(pointcloud_msg.points.end(), point);
    channels.values.insert(channels.values.end(), float((*sample_itr).WeightGet() * params.n_particles));
    //channels.values.insert(channels.values.end(), float(sample(5)));
  }

  pointcloud_msg.channels.insert(pointcloud_msg.channels.end(), channels);
  point_cloud_pub.publish(pointcloud_msg);
}

void PfNav::publishParticleCloud() {
  // publish a PointCloud of particle states and weights
  //ROS_INFO("This is where the particles will be published.");
  std::vector<BFL::WeightedSample<MatrixWrapper::ColumnVector>>::iterator sample_itr;
  std::vector<BFL::WeightedSample<MatrixWrapper::ColumnVector>> samples;
  samples = filter->PostGet()->ListOfSamplesGet();

  pf_nav_msgs::ParticleCloud particlecloud_msg;
  particlecloud_msg.header.frame_id = params.map_frame;
  particlecloud_msg.header.stamp = ros::Time::now();

  for (sample_itr = samples.begin(); sample_itr<samples.end(); sample_itr++) {
    pf_nav_msgs::Particle particle;
    MatrixWrapper::ColumnVector sample = (*sample_itr).ValueGet();
    particle.x = float(sample(1));
    particle.y = float(sample(2));
    particle.vx = float(sample(3));
    particle.vy = float(sample(4));
    particle.heading = float(sample(5));
    particle.weight = float((*sample_itr).WeightGet());
    particle.water_depth = 0.0;
    particlecloud_msg.particles.insert(particlecloud_msg.particles.end(), particle);
  }

  particle_cloud_pub.publish(particlecloud_msg);
}

void PfNav::publishOdometry() {
  // publishes a ROS odometry message of the expected value
  // expected value is not super good for complex distributions
  BFL::Pdf<MatrixWrapper::ColumnVector>* posterior = filter->PostGet();
  MatrixWrapper::ColumnVector post = posterior->ExpectedValueGet();
  MatrixWrapper::SymmetricMatrix post_cov = posterior->CovarianceGet();

  nav_msgs::Odometry odometry_msg;
  odometry_msg.header.frame_id = params.map_frame;
  odometry_msg.header.stamp = ros::Time::now();
  odometry_msg.pose.pose.position.x = post(1);
  odometry_msg.pose.pose.position.y = post(2);
  odometry_msg.pose.pose.position.z = last_vehicle_depth;
  odometry_msg.twist.twist.linear.x = post(3);
  odometry_msg.twist.twist.linear.y = post(4);
  tf2::Quaternion q;
  q.setRPY(0, 0, post(5) + last_yaw);
  odometry_msg.pose.pose.orientation.x = q.x();
  odometry_msg.pose.pose.orientation.y = q.y();
  odometry_msg.pose.pose.orientation.z = q.z();
  odometry_msg.pose.pose.orientation.w = q.w();

  // I drew a picture, trust me on these
  odometry_msg.pose.covariance[0] = post_cov(1,1) * 1; // scaled for EKF
  odometry_msg.pose.covariance[1] = post_cov(1,2) * 1;
  odometry_msg.pose.covariance[6] = post_cov(2,1) * 1;
  odometry_msg.pose.covariance[7] = post_cov(2,2) * 1;
  odometry_msg.pose.covariance[5] = post_cov(1,5);
  odometry_msg.pose.covariance[11] = post_cov(2,5);

  odometry_msg.twist.covariance[0] = post_cov(3,3);
  odometry_msg.twist.covariance[1] = post_cov(3,4);
  odometry_msg.twist.covariance[6] = post_cov(4,3);
  odometry_msg.twist.covariance[7] = post_cov(4,4);

  //ROS_INFO("pf_nav: t: %.3f, x: %.3f, y: %.3f", odometry_msg.header.stamp.toSec(), odometry_msg.pose.pose.position.x, odometry_msg.pose.pose.position.y);
  // Only publish if last range was valid
  if (last_range_valid) {
    mean_odometry_pub.publish(odometry_msg);
  }
}

void PfNav::publishMostLikelyOdometry() {
  // find a way to get the best particle, which probably means the particle closest to
  // the peak in the distribution
  // maybe histogram?
  ROS_INFO("This is where the maximum liklihood pose will be published.");
}
