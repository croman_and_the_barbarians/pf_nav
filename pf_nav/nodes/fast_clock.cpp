#include <ros/ros.h>
#include <pf_nav/fast_clock_class.h>

#include <rosgraph_msgs/Clock.h>

int main(int argc, char **argv) {
  ros::init(argc, argv, "fast_clock");
  ros::NodeHandle n;
  FastClock fast_clock(n);
  fast_clock.spin();
  return 0;
}
