#ifndef GRIDMAP_MAKER_CLASS_H
#define GRIDMAP_MAKER_CLASS_H

#include <ros/ros.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>
#include <pcl_ros/io/pcd_io.h>
#include <pcl/common/common.h>

#define PCD_FILENAME            "/home/dave/catkin_ws/src/pf_nav/pf_nav/sample_data/block_island_test.utm_local.pcd"
#define GRIDMAP_FILENAME        "/home/dave/catkin_ws/src/pf_nav/pf_nav/sample_data/block_island_test.bag"
#define GRIDMAP_RESOLUTION      200
#define OFFSET_X                0 // this number is added to X positions
#define OFFSET_Y                0 // this number is added to Y positions

class GridmapMaker {
public:
  ros::NodeHandle nh_;

  grid_map::GridMap map;
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;

  GridmapMaker(ros::NodeHandle &n);
  ~GridmapMaker();

  void loadFromFile(std::string filename, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud);

private:

};

#endif // GRIDMAP_MAKER_CLASS_H
