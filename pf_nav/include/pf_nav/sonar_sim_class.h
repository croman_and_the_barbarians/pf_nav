#ifndef SONAR_SIM_H
#define SONAR_SIM_H

#include <ros/ros.h>

// ros message types
#include <nav_msgs/Odometry.h> // subscribe odometry
#include <geometry_msgs/PoseWithCovarianceStamped.h> // depth
#include <pf_nav_msgs/RangeWithCovariance.h> // publish range altimeter

// grid map
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>

struct sonarSimParams {
  void getParams(ros::NodeHandle &nh) {
    nh.param<std::string>("gridmap_filename", gridmap_filename, "");
    nh.param<std::string>("gridmap_topic", gridmap_topic, "/grid_map");
    nh.param<std::string>("map_frame", map_frame, "world");
    nh.param<std::string>("range_topic", range_topic, "/sonar_sensor/range");
    nh.param<std::string>("vehicledepth_topic", vehicledepth_topic, "/eca_a9/depth");
    nh.param<std::string>("eca_odometry_topic", odometry_topic, "/eca_a9/pose_gt");
  }
  std::string gridmap_filename;
  std::string gridmap_topic;
  std::string map_frame;
  std::string range_topic;
  std::string vehicledepth_topic;
  std::string odometry_topic;
};

class SonarSim {
public:
  ros::NodeHandle nh_;

  SonarSim(ros::NodeHandle &n);
  ~SonarSim();

  void Init();

  void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg);
  void VehicleDepthCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);

  void publishRange();

  pf_nav_msgs::RangeWithCovariance range_msg;

private:
  grid_map::GridMap gridmap;

  sonarSimParams params;

  float last_vehicle_depth;
  ros::Time lastRangeTime;
  ros::Duration minTimeBetweenRangeMessages;

  ros::Subscriber vehicledepth_sub;
  ros::Subscriber odometry_sub;
  ros::Publisher range_pub;

};

#endif // SONAR_SIM_H
