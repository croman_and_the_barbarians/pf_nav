#include "pf_nav/gridmap_maker_class.h"

GridmapMaker::GridmapMaker(ros::NodeHandle &n) {
  nh_ = n;

  // load PCD cloud
  ROS_INFO("Loading %s.", PCD_FILENAME);
  loadFromFile(PCD_FILENAME, cloud);

  // create grid map
  ROS_INFO("Creating grid map.");
  grid_map::GridMap map({"elevation", "N", "SUM"});
  map.setFrameId("world");

  // set grid map size
  pcl::PointXYZ minPt, maxPt;
  pcl::getMinMax3D(*cloud, minPt, maxPt);
  float sizeX, sizeY, centerX, centerY;
  sizeX = (maxPt.x - minPt.x); sizeY = (maxPt.y - minPt.y);
  centerX = (minPt.x + maxPt.x)/2 + OFFSET_X; centerY = (minPt.y + maxPt.y)/2 + OFFSET_Y;
  map.setGeometry(grid_map::Length(sizeX, sizeY), GRIDMAP_RESOLUTION, grid_map::Position(centerX, centerY));

  ROS_INFO("Adding points to grid map.");
  // add each point to grid map
  for (size_t i=0; i < cloud->points.size(); ++i) {
    ROS_INFO_THROTTLE(1, "point %d of %d", int(i), int(cloud->points.size()));

    // find grid cell
    grid_map::Position grid_cell;
    grid_cell.x() = double(cloud->points[i].x + OFFSET_X);
    grid_cell.y() = double(cloud->points[i].y + OFFSET_Y);

    if (map.isInside(grid_cell)) {
      // initialize if its empty
      if (isnan(map.atPosition("N", grid_cell))) {
        map.atPosition("N", grid_cell) = 0;
        map.atPosition("SUM", grid_cell) = 0;
        map.atPosition("elevation", grid_cell) = 0;
      }

      // add point
      map.atPosition("N", grid_cell) = map.atPosition("N", grid_cell) + 1;
      map.atPosition("SUM", grid_cell) = map.atPosition("SUM", grid_cell) + cloud->points[i].z;
      map.atPosition("elevation", grid_cell) = map.atPosition("SUM", grid_cell) / map.atPosition("N", grid_cell);
    } // end grid cell is inside map
  } // end for each point

  ROS_INFO("Saving grid map.");
  grid_map::GridMapRosConverter::saveToBag(map, GRIDMAP_FILENAME, "/grid_map");
}

GridmapMaker::~GridmapMaker() {

}

void GridmapMaker::loadFromFile(std::string filename, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud) {
  cloud.reset(new pcl::PointCloud<pcl::PointXYZ>);
  if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename, *cloud) == -1) {
      PCL_ERROR("Could not read file.");
      return;
  }
}
