#include <ros/ros.h>
#include "pf_nav/fast_clock_class.h"


FastClock::FastClock(ros::NodeHandle &n) {
  nh_ = n;
  params.getParams(nh_);

  clock_pub = nh_.advertise<rosgraph_msgs::Clock>("/clock", 10, false);

  start_time = ros::Time::now();
  start_time_wall = ros::WallTime::now();
}

FastClock::~FastClock() {

}

void FastClock::spin() {
  ros::WallTime current_time_wall;
  ros::Time sim_time;
  ros::WallDuration wall_elapsed;
  ros::Duration sim_elapsed;
  rosgraph_msgs::Clock clock;

  ros::WallRate loop_rate(1000.);

  while (ros::ok()) {
    current_time_wall = ros::WallTime::now();
    wall_elapsed = current_time_wall - start_time_wall;
    sim_elapsed.fromSec(wall_elapsed.toSec() * params.clock_rate);
    sim_time = start_time + sim_elapsed;
    clock.clock = sim_time;
    clock_pub.publish(clock);
    loop_rate.sleep();
  }
}
