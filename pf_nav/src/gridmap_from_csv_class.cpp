#include "pf_nav/gridmap_from_csv_class.h"

GridmapMakerCsv::GridmapMakerCsv(ros::NodeHandle &n) {
  nh_ = n;
  params.getParams(nh_);

  // load CSV cloud
  ROS_INFO("Loading %s.", params.csv_filename);
  loadFromFile(params.csv_filename, cloud);

  // create grid map
  ROS_INFO("Creating grid map.");
  grid_map::GridMap map({"elevation", "N", "SUM"});
  map.setFrameId("world");

  // set grid map size
  pcl::PointXYZ minPt, maxPt;
  pcl::getMinMax3D(*cloud, minPt, maxPt);
  float sizeX, sizeY, centerX, centerY;
  sizeX = (maxPt.x - minPt.x); sizeY = (maxPt.y - minPt.y);
  centerX = (minPt.x + maxPt.x)/2 + params.offset_x; centerY = (minPt.y + maxPt.y)/2 + params.offset_y;
  map.setGeometry(grid_map::Length(sizeX, sizeY), params.gridmap_resolution, grid_map::Position(centerX, centerY));

  ROS_INFO("Adding points to grid map.");
  // add each point to grid map
  for (size_t i=0; i < cloud->points.size(); ++i) {
    ROS_INFO_THROTTLE(0.1, "point %d of %d", int(i), int(cloud->points.size()));

    // find grid cell
    grid_map::Position grid_cell;
    grid_cell.x() = double(cloud->points[i].x + params.offset_x);
    grid_cell.y() = double(cloud->points[i].y + params.offset_y);

    if (map.isInside(grid_cell)) {
      // initialize if its empty
      if (isnan(map.atPosition("N", grid_cell))) {
        map.atPosition("N", grid_cell) = 0;
        map.atPosition("SUM", grid_cell) = 0;
        map.atPosition("elevation", grid_cell) = 0;
      }

      // add point
      map.atPosition("N", grid_cell) = map.atPosition("N", grid_cell) + 1;
      map.atPosition("SUM", grid_cell) = map.atPosition("SUM", grid_cell) + cloud->points[i].z;
      map.atPosition("elevation", grid_cell) = map.atPosition("SUM", grid_cell) / map.atPosition("N", grid_cell);
    } // end grid cell is inside map
  } // end for each point

  ROS_INFO("Saving grid map.");
  grid_map::GridMapRosConverter::saveToBag(map, params.gridmap_filename, "/grid_map");
}

GridmapMakerCsv::~GridmapMakerCsv() {

}

void GridmapMakerCsv::loadFromFile(std::string filename, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud) {
  cloud.reset(new pcl::PointCloud<pcl::PointXYZ>);
  std::fstream file;
  file.open(filename);
  std::string line;
  double x, y, z;
  while (getline(file, line, '\n')) {
    sscanf(line.c_str(), "%lf,%lf,%lf", &x, &y, &z);
    //ROS_INFO("%f,%f,%f,", x, y, z);
    pcl::PointXYZ d(x, y, z);
    cloud->points.push_back(d);
  }
  file.close();

}
