#ifndef PF_NAV_H
#define PF_NAV_H

#include <ros/ros.h>
#include <math.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Vector3.h>
#include <tf2/LinearMath/Transform.h>
#include <tf2/transform_datatypes.h>
//#include <tf2_ros/transform_listener.h>

#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Vector3.h>

// ros message types
#include <nav_msgs/Odometry.h> // publish particles
#include <geometry_msgs/TwistWithCovarianceStamped.h> // subscribe odometry
#include <pf_nav_msgs/RangeWithCovariance.h>
#include <pf_nav_msgs/RangeWithCovariance.h> // subscribe altimeter
#include <sensor_msgs/PointCloud.h> // publish particles as pointcloud
#include <geometry_msgs/Point32.h>
#include <sensor_msgs/ChannelFloat32.h>

#include <pf_nav_msgs/Particle.h>
#include <pf_nav_msgs/ParticleCloud.h>

//#include <geometry_msgs/PoseWithCovarianceStamped.h> // subscribe USBL, depth
//#include <pf_nav_msgs/TwistyPose.h> // publish particles
//#include <pf_nav_msgs/TwistyPoseArray.h> // publish particles
#include <sensor_msgs/Imu.h> // IMU (heading)
//#include <float_sensor_msgs/Depth.h> // ACFR vehicle depth message
#include <sensor_msgs/Range.h> // Sonar range
#include <geometry_msgs/PoseWithCovarianceStamped.h>

// particle filter stuff
#include <bfl/pdf/conditionalpdf.h>
#include <bfl/pdf/gaussian.h>
#include <bfl/filter/bootstrapfilter.h>

// grid map
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>

#define N_STATES                5    // x pos, y pos, x vel, y vel, heading

struct pfNavParams {
  void getParams(ros::NodeHandle &nh) {
    nh.param<double>("delta_t", delta_t, 1.0);
    int n_particles_int;
    nh.param<int>("n_particles", n_particles_int, 100000);
    n_particles = uint(n_particles_int);
    nh.param<std::string>("gridmap_filename", gridmap_filename, "");
    nh.param<std::string>("gridmap_topic", gridmap_topic, "/grid_map");
    nh.param<std::string>("map_frame", map_frame, "map");
    nh.param<std::string>("visualodom_topic", visualodom_topic, "/visual_odometry");
    nh.param<std::string>("range_topic", range_topic, "/range");
    nh.param<std::string>("vehicledepth_topic", vehicledepth_topic, "/vehicle_depth");
    nh.param<std::string>("imu_topic", imu_topic, "/imu");
    nh.param<std::string>("dvl_topic", dvl_topic, "/dvl");
    nh.param<std::string>("pointcloud_topic", pointcloud_topic, "/pointcloud");
    nh.param<std::string>("particlecloud_topic", particlecloud_topic, "/particlecloud");
    nh.param<std::string>("odometry_topic", odometry_topic, "/odometry");
    nh.param<std::string>("usbl_topic", usbl_topic, "/usbl");
    nh.param<std::string>("deadreckon_topic", deadreckon_topic, "/eca_a9/vel");
    nh.param<bool>("use_visualodom", use_visualodom, true);
    nh.param<bool>("use_range", use_range, true);
    nh.param<bool>("use_vehicledepth", use_vehicledepth, true);
    nh.param<bool>("use_imu", use_imu, true);
    nh.param<bool>("use_dvl", use_dvl, false);
    nh.param<bool>("use_acfrrange", use_acfrrange, false);
    nh.param<bool>("use_acfrvisualodom", use_acfrvisualodom, false);
    nh.param<bool>("use_usbl", use_usbl, true);
    nh.param<bool>("use_deadreckon", use_deadreckon, true);
    nh.param<double>("heading_offset", heading_offset, 0.0);
    if (!nh.getParam("sys_noise_mu", sys_noise_mu)) ROS_WARN("No default sys_noise_mu");
    if (!nh.getParam("sys_noise_cov", sys_noise_cov)) ROS_WARN("No default sys_noise_cov");
    if (!nh.getParam("prior_mu", prior_mu)) ROS_WARN("No default prior_mu");
    if (!nh.getParam("prior_cov", prior_cov)) ROS_WARN("No default prior_cov");
  }
  double delta_t;
  uint n_particles;
  std::string gridmap_filename;
  std::string gridmap_topic;
  std::string map_frame;
  std::string visualodom_topic;
  std::string range_topic;
  std::string vehicledepth_topic;
  std::string imu_topic;
  std::string dvl_topic;
  std::string pointcloud_topic;
  std::string particlecloud_topic;
  std::string odometry_topic;
  std::string usbl_topic;
  std::string deadreckon_topic;
  bool use_visualodom;
  bool use_range;
  bool use_imu;
  bool use_dvl;
  bool use_vehicledepth;
  bool use_acfrrange;
  bool use_acfrvisualodom;
  bool use_usbl;
  bool use_deadreckon;
  double heading_offset;
  std::vector<double> sys_noise_mu, sys_noise_cov;
  std::vector<double> prior_mu, prior_cov;
};

// globals used in measurements and updates, switch to singletons?
extern double dt;
extern double hdg_offset;
extern grid_map::GridMap gridmap;
extern double last_vehicle_depth;
//extern double last_accel_x;
//extern double last_accel_y;
extern double last_yaw;
extern double last_vx;
extern double last_vy;
extern double last_vx_var;
extern double last_vy_var;
extern double last_yaw_var;

namespace BFL {

  class VehicleModel: public BFL::ConditionalPdf<MatrixWrapper::ColumnVector,MatrixWrapper::ColumnVector> {
  public:
    VehicleModel(const BFL::Gaussian& additiveNoise);
    virtual ~VehicleModel();
    virtual bool SampleFrom(BFL::Sample<MatrixWrapper::ColumnVector>& one_sample, int method=DEFAULT, void* args=nullptr) const;

  private:
    BFL::Gaussian _additiveNoise;
  }; // end class VehicleModel


  class UsblPdf: public BFL::ConditionalPdf<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector> {
  public:
    UsblPdf(const Gaussian& additiveNoise): BFL::ConditionalPdf<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector> (2, 1) {
      _measurementNoise = additiveNoise;
    }
    virtual ~UsblPdf() { }

    virtual BFL::Probability ProbabilityGet(const MatrixWrapper::ColumnVector& measurement) const;
  private:
    BFL::Gaussian _measurementNoise;
  }; // end UsblPdf


  class VisualOdometryPdf: public BFL::ConditionalPdf<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector> {
  public:
    VisualOdometryPdf(const Gaussian& additiveNoise): BFL::ConditionalPdf<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector> (2, 1) {
      _measurementNoise = additiveNoise;
    }
    virtual ~VisualOdometryPdf() { }

    virtual BFL::Probability ProbabilityGet(const MatrixWrapper::ColumnVector& measurement) const;
  private:
    BFL::Gaussian _measurementNoise;
  }; // end VisualOdometryPdf

  class ImuPdf: public BFL::ConditionalPdf<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector> {
  public:
    ImuPdf(const Gaussian& additiveNoise): BFL::ConditionalPdf<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector> (1, 1) {
      _measurementNoise = additiveNoise;
    }
    virtual ~ImuPdf() { }

    virtual BFL::Probability ProbabilityGet(const MatrixWrapper::ColumnVector& measurement) const;
  private:
    BFL::Gaussian _measurementNoise;
  }; // end ImuPdf

  class RangePdf: public BFL::ConditionalPdf<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector> {
  public:
    RangePdf(const Gaussian& additiveNoise): BFL::ConditionalPdf<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector> (1, 1) {
      _measurementNoise = additiveNoise;
    }
    virtual ~RangePdf() { }

    virtual BFL::Probability ProbabilityGet(const MatrixWrapper::ColumnVector& measurement) const;
  private:
    BFL::Gaussian _measurementNoise;
  }; // end RangePdf

}; // end namespace BFL


class PfNav {
public:
  ros::NodeHandle nh_;

  BFL::VehicleModel* sys_pdf;
  BFL::SystemModel<MatrixWrapper::ColumnVector>* sys_model;
  BFL::MCPdf<MatrixWrapper::ColumnVector>* prior_discr;
  BFL::BootstrapFilter<MatrixWrapper::ColumnVector,MatrixWrapper::ColumnVector>* filter;

  PfNav(ros::NodeHandle &n);
  ~PfNav();

  void InitFilter();

  void timerCallback(const ros::TimerEvent& event);
  //void USBLCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
  //void DepthCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
  //void AltitudeCallback(const pf_nav_msgs::RangeWithCovariance::ConstPtr& msg);

  void UsblCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
  void VisualOdometryCallback(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& msg);
  void RangeCallback(const pf_nav_msgs::RangeWithCovariance::ConstPtr& msg);
  void ACFRRangeCallback(const sensor_msgs::Range::ConstPtr& msg);
  void ACFRVisualOdometryCallback(const nav_msgs::Odometry::ConstPtr& msg);
  //void VehicleDepthCallback(const float_sensor_msgs::Depth::ConstPtr& msg);
  void VehicleDepthCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
  void ImuCallback(const sensor_msgs::Imu::ConstPtr& msg);
  void DvlCallback(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& msg);
  void DeadreckonCallback(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& msg);

  void publishParticles(); // publish all particles as a pointcloud
  void publishParticleCloud(); // publish all particles as a ParticleCloud
  void publishOdometry(); // publish the mean pose as Odometry
  void publishMostLikelyOdometry(); // publish the maximum likelihood pose with Odometry

private:
  pfNavParams params;

  //grid_map::GridMap gridmap;
  grid_map_msgs::GridMap gridmap_msg;
  ros::Publisher gridmap_pub;

  //tf2_ros::Buffer       tf_buffer_;
  //tf2_ros::TransformListener *tf_listener_;

  ros::Time lastUpdate;
  ros::Timer timer_for_callback;
  bool last_range_valid;

  ros::Time lastImuUpdate;
  ros::Time lastWaterdepthUpdate;
  ros::Time lastDvlUpdate;
  ros::Duration waterdepthInterval;
  ros::Duration imuInterval;
  ros::Duration dvlInterval;

  BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>* usbl_measurement_model;
  BFL::UsblPdf* usbl_pdf;

  BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>* visual_odometry_measurement_model;
  BFL::VisualOdometryPdf* visual_odometry_pdf;

  BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>* range_measurement_model;
  BFL::RangePdf* range_pdf;

  BFL::MeasurementModel<MatrixWrapper::ColumnVector, MatrixWrapper::ColumnVector>* imu_measurement_model;
  BFL::ImuPdf* imu_pdf;

  ros::Subscriber usbl_sub;
  ros::Subscriber vehicledepth_sub;
  //ros::Subscriber altitude_sub;
  ros::Subscriber visual_odometry_sub;
  ros::Subscriber range_sub;
  ros::Subscriber imu_sub;
  ros::Subscriber dvl_sub;
  ros::Subscriber deadreckon_sub;

  ros::Publisher particle_cloud_pub;
  ros::Publisher point_cloud_pub;
  ros::Publisher mean_odometry_pub;
  ros::Publisher most_likely_odometry_pub;

};

#endif // PF_NAV_H
