#ifndef PF_SIM_CLASS_H
#define PF_SIM_CLASS_H

#include <ros/ros.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>
#include "pf_nav/spline.h"

#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>
#include <pf_nav_msgs/RangeWithCovariance.h>

#include <bfl/pdf/gaussian.h>

struct pfSimParams {
  void getParams(ros::NodeHandle &nh) {
    nh.param<double>("delta_t", delta_t, 1.0);
    nh.param<std::string>("gridmap_filename", gridmap_filename, "");
    nh.param<std::string>("gridmap_topic", gridmap_topic, "/grid_map");
    nh.param<std::string>("map_frame", map_frame, "map");
    nh.param<std::string>("groundtruth_topic", groundtruth_topic, "/ground_truth");
    nh.param<std::string>("visualodom_topic", visualodom_topic, "/visual_odometry");
    nh.param<std::string>("range_topic", range_topic, "/range");
    if (!nh.getParam("visualodom_cov", visualodom_cov)) ROS_WARN("No default visualodom_cov");
    if (!nh.getParam("visualodom_corruption_mu", visualodom_corruption_mu)) ROS_WARN("No default visualodom_corruption_mu");
    if (!nh.getParam("visualodom_corruption_cov", visualodom_corruption_cov)) ROS_WARN("No default visualodom_corruption_cov");
    if (!nh.getParam("range_cov", range_cov)) ROS_WARN("No default range_cov");
    if (!nh.getParam("range_corruption_mu", range_corruption_mu)) ROS_WARN("No default range_corruption_mu");
    if (!nh.getParam("range_corruption_cov", range_corruption_cov)) ROS_WARN("No default range_corruption_cov");
    if (!nh.getParam("path_t", path_t)) ROS_ERROR("No default path_t");
    if (!nh.getParam("path_x", path_x)) ROS_ERROR("No default path_x");
    if (!nh.getParam("path_y", path_y)) ROS_ERROR("No default path_y");
    if (!nh.getParam("path_hdg", path_hdg)) ROS_ERROR("No default path_hdg");
  }

  double delta_t;
  std::string gridmap_filename;
  std::string gridmap_topic;
  std::string map_frame;
  std::string groundtruth_topic;
  std::string visualodom_topic;
  std::string range_topic;
  std::vector<double> visualodom_cov, visualodom_corruption_mu, visualodom_corruption_cov;
  std::vector<double> range_cov, range_corruption_mu, range_corruption_cov;
  std::vector<double> path_t, path_x, path_y, path_hdg;
};

class PfSim {
public:
  ros::NodeHandle nh_;

  PfSim(ros::NodeHandle &n);
  ~PfSim();

  void spin();
  void spin_once();

  void timerCallback();

  nav_msgs::Odometry getOdometry(); // returns the simulated vehicle's odometry at ros::Time::now()


private:
  pfSimParams params;

  ros::Time start_time;
  grid_map::GridMap gridmap;
  grid_map_msgs::GridMap gridmap_msg;
  ros::Publisher gridmap_pub;

  std::vector<double> path_t, path_x, path_y, path_hdg;
  tk::spline spline_x, spline_y, spline_hdg;
  ros::Publisher groundtruth_pub;

  // fake messages
  ros::Publisher visualodometry_pub;
  ros::Publisher range_pub;

  BFL::Gaussian* odom_corruption;
  BFL::Gaussian* range_corruption;

  // ground truth
  ros::Publisher odometry_pub;
  //ros::Time lastUpdate;
};



#endif // PF_SIM_CLASS_H
