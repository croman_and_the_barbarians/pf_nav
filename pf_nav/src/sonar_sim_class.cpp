#include "pf_nav/sonar_sim_class.h"

SonarSim::SonarSim(ros::NodeHandle &n) {
  nh_ = n;
  params.getParams(nh_);

  // load grid map
  ROS_INFO("Loading %s", params.gridmap_filename.c_str());
  grid_map::GridMapRosConverter::loadFromBag(params.gridmap_filename, params.gridmap_topic, gridmap);
}

void SonarSim::Init() {
  minTimeBetweenRangeMessages.fromSec(1.0);
  //ROS_INFO("Initializationg subscribers and publishers.");
  // ros subscribers and publishers
  vehicledepth_sub = nh_.subscribe(params.vehicledepth_topic, 10, &SonarSim::VehicleDepthCallback, this);
  odometry_sub = nh_.subscribe(params.odometry_topic, 10, &SonarSim::OdometryCallback, this);
  //ROS_INFO("Subscribing to %s", params.odometry_topic.c_str());
  range_pub = nh_.advertise<pf_nav_msgs::RangeWithCovariance>(params.range_topic, 10);
}

SonarSim::~SonarSim() {
}

void SonarSim::OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  //ROS_INFO("Odometry callback.");
  // check if enough time has passed.
  if ((msg->header.stamp - lastRangeTime) > minTimeBetweenRangeMessages) {
    double x = msg->pose.pose.position.x;
    double y = msg->pose.pose.position.y;

    grid_map::Position position(x, y);
    float waterdepth;
    if (gridmap.isInside(position)) { // segfault preventer
      waterdepth = gridmap.atPosition("elevation", position, grid_map::InterpolationMethods::INTER_LINEAR);
    }
    else {
      waterdepth = -1.0;
    }
    if (isnan(waterdepth)) {
      waterdepth = -1.0;
    }

    float range = -waterdepth + last_vehicle_depth;

    ROS_INFO("Position: (%.2f, %.2f)  Waterdepth: %.2f  Vehicledepth: %.2f  Range: %.2f", x, y, waterdepth, last_vehicle_depth, range);
    range_msg.range.header.stamp = ros::Time::now();
    range_msg.range.radiation_type = range_msg.range.ULTRASOUND;
    range_msg.range.field_of_view = 0.1;
    range_msg.range.min_range = 0.001;
    range_msg.range.max_range = 1000;
    range_msg.range.range = range;
    range_msg.covariance = 0.1;

    range_pub.publish(range_msg);
    lastRangeTime = msg->header.stamp;
  }

}

void SonarSim::VehicleDepthCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg) {
  //ROS_INFO("Vehicle depth callback.");
  last_vehicle_depth = msg->pose.pose.position.z;
}
