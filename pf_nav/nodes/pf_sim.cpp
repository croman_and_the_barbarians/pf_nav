#include <ros/ros.h>
#include <pf_nav/pf_sim_class.h>

int main(int argc, char **argv) {
  ros::init(argc, argv, "pf_sim");
  ros::NodeHandle n;
  PfSim pf_sim(n);
  pf_sim.spin();
  return 0;
}
