#include "pf_nav/pf_sim_class.h"
#include <tf2/LinearMath/Quaternion.h>
#include <math.h>


PfSim::PfSim(ros::NodeHandle &n) {
  nh_ = n;
  params.getParams(nh_);

  // load grid map
  grid_map::GridMapRosConverter::loadFromBag(params.gridmap_filename, params.gridmap_topic, gridmap);

  // publish grid map to latched topic
  // this is now done by pf_nav_class
  //gridmap.setTimestamp(ros::Time::now().toNSec());
  //grid_map::GridMapRosConverter::toMessage(gridmap, gridmap_msg);
  //gridmap_pub = nh_.advertise<grid_map_msgs::GridMap>(params.gridmap_topic, 1, true);
  //gridmap_pub.publish(gridmap_msg);

  // ground-truth odometry publisher
  groundtruth_pub = nh_.advertise<nav_msgs::Odometry>(params.groundtruth_topic, 10, false);

  // fake sensor publishers
  visualodometry_pub = nh_.advertise<geometry_msgs::TwistWithCovarianceStamped>(params.visualodom_topic, 10, false);
  range_pub = nh_.advertise<pf_nav_msgs::RangeWithCovariance>(params.range_topic, 10, false);

  // fake sensor setup
  MatrixWrapper::ColumnVector odom_corruption_mu(2);
  odom_corruption_mu = 0.0;
  odom_corruption_mu(1) = params.visualodom_corruption_mu[0];
  odom_corruption_mu(2) = params.visualodom_corruption_mu[1];
  MatrixWrapper::SymmetricMatrix odom_corruption_cov(2);
  odom_corruption_cov = 0.0;
  odom_corruption_cov(1,1) = params.visualodom_corruption_cov[0];
  odom_corruption_cov(1,2) = params.visualodom_corruption_cov[1];
  odom_corruption_cov(2,1) = params.visualodom_corruption_cov[2];
  odom_corruption_cov(2,2) = params.visualodom_corruption_cov[3];
  odom_corruption = new BFL::Gaussian(odom_corruption_mu, odom_corruption_cov);

  MatrixWrapper::ColumnVector range_corruption_mu(1);
  range_corruption_mu = 0.0;
  range_corruption_mu(1) = params.range_corruption_mu[0];
  MatrixWrapper::SymmetricMatrix range_corruption_cov(1);
  range_corruption_cov = 0.0;
  range_corruption_cov(1,1) = params.range_corruption_cov[0];
  range_corruption = new BFL::Gaussian(range_corruption_mu, range_corruption_cov);

  // initialize ground-truth
  spline_x.set_points(params.path_t, params.path_x);
  spline_y.set_points(params.path_t, params.path_y);
  spline_hdg.set_points(params.path_t, params.path_hdg);

  start_time = ros::Time::now();
}

PfSim::~PfSim() {

}

nav_msgs::Odometry PfSim::getOdometry() {
  nav_msgs::Odometry msg;
  msg.header.frame_id = params.map_frame;
  msg.header.stamp = ros::Time::now();

  ros::Duration elapsed_time = msg.header.stamp - start_time;
  double elapsed_time_s = elapsed_time.toSec();

  msg.pose.pose.position.x = spline_x(elapsed_time_s);
  msg.pose.pose.position.y = spline_y(elapsed_time_s);
  msg.pose.pose.position.z = 0;

  tf2::Quaternion q;
  q.setRPY(0, 0, (90-spline_hdg(elapsed_time_s)) * M_PI / 180.);
  msg.pose.pose.orientation.x = q.x();
  msg.pose.pose.orientation.y = q.y();
  msg.pose.pose.orientation.z = q.z();
  msg.pose.pose.orientation.w = q.w();

  double dx = spline_x(elapsed_time_s + params.delta_t) - spline_x(elapsed_time_s);
  double dy = spline_y(elapsed_time_s + params.delta_t) - spline_y(elapsed_time_s);
  msg.twist.twist.linear.x = dx / params.delta_t;
  msg.twist.twist.linear.y = dy / params.delta_t;
  msg.twist.twist.linear.z = 0;

  return msg;
}

void PfSim::timerCallback() {
  // get the real position
  nav_msgs::Odometry odom = getOdometry();
  groundtruth_pub.publish(odom);

  // simulate some sensors
  // visual odom, straight from ground truth
  // get noise
  std::vector<BFL::Sample<MatrixWrapper::ColumnVector>> odom_corruption_sample; // 2 states, 1 sample
  odom_corruption->SampleFrom(odom_corruption_sample, 1);
  MatrixWrapper::ColumnVector odom_corruption_value = odom_corruption_sample.begin()->ValueGet();

  geometry_msgs::TwistWithCovarianceStamped visualodom;
  visualodom.header = odom.header;
  visualodom.twist.twist.linear.x = odom.twist.twist.linear.x + odom_corruption_value(1);
  visualodom.twist.twist.linear.y = odom.twist.twist.linear.y + odom_corruption_value(2);
  visualodom.twist.covariance[0] = params.visualodom_cov[0];
  visualodom.twist.covariance[1] = params.visualodom_cov[1];
  visualodom.twist.covariance[6] = params.visualodom_cov[2];
  visualodom.twist.covariance[7] = params.visualodom_cov[3];
  visualodometry_pub.publish(visualodom);

  // range measurement, query map
  grid_map::Position position(odom.pose.pose.position.x, odom.pose.pose.position.y);
  float waterdepth;
  if (gridmap.isInside(position)) {
    waterdepth = gridmap.atPosition("elevation", position, grid_map::InterpolationMethods::INTER_LINEAR);
  }
  else {
    waterdepth = 0.0;
  }

  // get noise
  std::vector<BFL::Sample<MatrixWrapper::ColumnVector>> range_corruption_sample;
  range_corruption->SampleFrom(range_corruption_sample, 1);
  MatrixWrapper::ColumnVector range_corruption_value = range_corruption_sample.begin()->ValueGet();

  pf_nav_msgs::RangeWithCovariance rangewithcov;
  rangewithcov.range.header = odom.header;
  rangewithcov.range.range = -waterdepth + float(range_corruption_value(1));
  rangewithcov.covariance = float(params.range_cov[0]);
  range_pub.publish(rangewithcov);

  ROS_INFO("pf_sim: t: %.3f, x: %.3f, y: %.3f, depth: %.3f", odom.header.stamp.toSec(),
           odom.pose.pose.position.x, odom.pose.pose.position.y, -double(waterdepth));
}

void PfSim::spin_once() {
  timerCallback();
}

void PfSim::spin() {
  ros::Rate loop_rate(1.0 / params.delta_t); // in hz
  while (ros::ok()) {
    spin_once();
    loop_rate.sleep();
  }
}
