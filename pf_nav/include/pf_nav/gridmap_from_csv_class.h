#ifndef GRIDMAP_FROM_CSV_CLASS_H
#define GRIDMAP_FROM_CSV_CLASS_H

#include <ros/ros.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>
#include <pcl_ros/io/pcd_io.h>
#include <pcl/common/common.h>

//#define CSV_FILENAME            "/home/dave/catkin_ws/src/pf_nav/pf_nav/sample_data/block_island_test.utm_local.csv"
//#define GRIDMAP_FILENAME        "/home/dave/catkin_ws/src/pf_nav/pf_nav/sample_data/block_island_test.bag"
//#define GRIDMAP_RESOLUTION      200
//#define OFFSET_X                0 // this number is added to X positions
//#define OFFSET_Y                0 // this number is added to Y positions

struct gridmap_from_csvParams {
  void getParams(ros::NodeHandle &nh) {
    nh.param<double>("gridmap_resolution", gridmap_resolution, 0.0);
    nh.param<double>("offset_x", offset_x, 0.0);
    nh.param<double>("offset_y", offset_y, 0.0);
    nh.param<std::string>("gridmap_filename", gridmap_filename, "");
    nh.param<std::string>("csv_filename", csv_filename, "");
  }
  double gridmap_resolution, offset_x, offset_y;
  std::string csv_filename, gridmap_filename;
};

class GridmapMakerCsv {
public:
  ros::NodeHandle nh_;

  grid_map::GridMap map;
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;

  GridmapMakerCsv(ros::NodeHandle &n);
  ~GridmapMakerCsv();

  void loadFromFile(std::string filename, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud);

private:
  gridmap_from_csvParams params;

};

#endif // GRIDMAP_FROM_CSV_CLASS_H
