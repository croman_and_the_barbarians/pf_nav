#include <ros/ros.h>
#include <pf_nav/pf_nav_class.h>

int main(int argc, char **argv) {
  ros::init(argc, argv, "pf_nav");
  ros::NodeHandle n;
  PfNav pf_nav(n);
  if (ros::Time::isSimTime()) {
      ros::Time::waitForValid();
  }
  // test if sim, wait for time > 0
  pf_nav.InitFilter();
  ros::spin();
  return 0;
}
