# README #

pf_nav uses the Orocos Bayesian Filtering Library (BFL) to improve underwater vehicle navigation. The package requires Robot Operating System (ROS) version Melodic (built for Ubuntu 18.04) or later. Inputs and outputs are standard ROS message types.

### Dependencies ###

* ros-melodic-desktop
* ros-melodic-bfl
* ros-melodic-grid-map
* ros-melodic-grid-map

### Build Instructions ###

1. Create a directory ~/catkin_ws/src
2. Add the following two lines to ~/.bashrc:
    source /opt/ros/melodic/setup.bash
    source ~/catkin_ws/devel/setup.bash
3. Clone pf_nav into ~/catkin_ws/src
4. cd ~/catkin_ws
5. catkin_make

### Configuration ###

* Configure input and output topics, and which sensor sources to use, in pf_nav/pf_nav/config. For an example, see uuvsim.yaml.
* Configure launch file in pf_nav/pf_nav/launch. For an example, see uuvsim.launch. Or, add to the launch file for your vehicle's navigation system.
* The output odometry message can be incorporated into a full navigation solution, eg. Robot Localization. It is not in general appropriate to use the particle filter's output directly, as it does not include correct values for some fields including angular rates.

### Who do I talk to? ###

* Written and maintained by David Casagrande, Roman Lab, University of Rhode Island's Graduate School of Oceanography.