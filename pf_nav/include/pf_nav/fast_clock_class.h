#ifndef FAST_CLOCK_CLASS_H
#define FAST_CLOCK_CLASS_H

#include <ros/ros.h>
#include <rosgraph_msgs/Clock.h>

struct fastClockParams {
  void getParams(ros::NodeHandle &nh) {
    nh.param<double>("clock_rate", clock_rate, 1.0);
  }
  double clock_rate;
};

class FastClock {
public:
  ros::NodeHandle nh_;

  FastClock(ros::NodeHandle &n);
  ~FastClock();

  void spin();

private:
  fastClockParams params;

  ros::Time start_time;
  ros::WallTime start_time_wall;
  ros::Publisher clock_pub;
};

#endif // FAST_CLOCK_CLASS_H
